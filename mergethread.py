
from multiprocessing import Process, Pipe, cpu_count
import time, random, sys
import threading
import Person



def main():

	print("CPU count is: ", cpu_count())
	n = int(input("Input number of processes:\n"))
	temp1 = input("What do you want to sort? objects or integers array? or type 'exit' to exit\n")
	if (temp1.lower() == 'objects'):
		lyst = list()
		for x in range(10):
			height_random = random.choice(range(100, 200))
			weight_random = random.choice(range(20, 100))
			age_random = random.choice(range(20, 100))
			lyst.append(Person.Person(height_random, weight_random, age_random))
		pconn, cconn = Pipe()
		p = threading.Thread(target=mergeSortParallel, args=(lyst, cconn, n))
		p.start()
		lyst = pconn.recv()
	    
		p.join()
		print("\n\n", lyst)
        
	elif temp1.lower() == 'integers':
		lyst = random.sample(range(10000), 50)
		pconn, cconn = Pipe()
		p = threading.Thread(target=mergeSortParallel, args=(lyst, cconn, n))
		p.start()
		lyst = pconn.recv()
		p.join()
		print(lyst)
        
	elif temp1.lower() == 'exit':
		print("See ya! :)")

def merge(left, right):
	ret = []
	li = ri = 0
	while li < len(left) and ri < len(right):
		if left[li] < right[ri]:
			ret.append(left[li])
			li += 1
		else:
			ret.append(right[ri])
			ri += 1


	if (li == len(left)):
		ret.extend(right[ri:])
	else:
		ret.extend(left[li:])
	return ret

def mergesort(lyst):
	if len(lyst) <= 1:
		return lyst

	ind = len(lyst)//2
	return merge(mergesort(lyst[:ind]), mergesort(lyst[ind:]))


def mergeSortParallel(lyst, conn, procNum):

	if procNum <= 0 or len(lyst) <= 1:
		conn.send(mergesort(lyst))
		conn.close()
		return

	ind = len(lyst)//2

	pconnLeft, cconnLeft = Pipe()
	leftProc = threading.Thread(target=mergeSortParallel, args=(lyst[:ind], cconnLeft, procNum - 1))


	pconnRight, cconnRight = Pipe()
	rightProc = threading.Thread(target=mergeSortParallel, args=(lyst[ind:], cconnRight, procNum - 1))

	leftProc.start()
	rightProc.start()

	conn.send(merge(pconnLeft.recv(), pconnRight.recv()))
	conn.close()

	leftProc.join()
	rightProc.join()

if __name__ == '__main__':
	main()