import threading
import multiprocessing
import random
import time
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style
from pytictoc import TicToc
from matplotlib.animation import FuncAnimation

class RepeatedTimer(object):
  def __init__(self, interval, function, *args, **kwargs):
    self._timer = None
    self.interval = interval
    self.function = function
    self.args = args
    self.kwargs = kwargs
    self.is_running = False
    self.next_call = time.time()
    self.start()

  def _run(self):
    self.is_running = False
    self.start()
    self.function(*self.args, **self.kwargs)

  def start(self):
    if not self.is_running:
      self.next_call += self.interval
      self._timer = threading.Timer(self.next_call - time.time(), self._run)
      self._timer.start()
      self.is_running = True

  def stop(self):
    self._timer.cancel()
    self.is_running = False

style.use('fivethirtyeight')

def insertion_sort(InputList):
    for i in range(1, len(InputList)):
        j = i-1
        nxt_element = InputList[i]
        
        while (InputList[j] > nxt_element) and (j >= 0):
            InputList[j+1] = InputList[j]
            j=j-1
        InputList[j+1] = nxt_element

def bubblesort(list):

    for iter_num in range(len(list)-1,0,-1):
        for idx in range(iter_num):
            if list[idx]>list[idx+1]:
                temp = list[idx]
                list[idx] = list[idx+1]
                list[idx+1] = temp

def selection_sort(input_list):

    for idx in range(len(input_list)):
        min_idx = idx

        for j in range( idx +1, len(input_list)):
            if input_list[min_idx] > input_list[j]:
                min_idx = j

        input_list[idx], input_list[min_idx] = input_list[min_idx], input_list[idx]

lyst1 = random.sample(range(100000),10000)
lyst2 = []
lyst3 = []

for a in range(len(lyst1)):
    lyst2.append(lyst1[a])
    lyst3.append(lyst1[a])

x_vals = []
y_vals = []
processes = []

def hello():
    y_vals.append(threading.active_count())
    x_vals.append(time.perf_counter())

print("wait for a while ... ")

rt = RepeatedTimer(1, hello) # it auto-starts, no need of rt.start()
try:
    a = time.perf_counter()

    x = threading.Thread(target=insertion_sort, args=(lyst1,))
    processes.append(x)
    x.start()

    y = threading.Thread(target=bubblesort, args=(lyst2,))
    processes.append(y)
    y.start()

    z = threading.Thread(target=selection_sort, args=(lyst3,))
    processes.append(z)
    z.start()


    for process in processes:
        process.join()

    b = time.perf_counter()
finally:
    rt.stop() # better in a try/finally block to make sure the program ends!
 

for i in range(len(x_vals)):
    plt.scatter(x_vals[i], y_vals[i])
    plt.pause(1)

plt.show()



