import random, time, sys
from multiprocessing import Process, Pipe, cpu_count
import Person


def main():
    print("CPU count is: ", cpu_count())
    n = int(input("Input number of processes:\n"))
    temp1 = input("What do you want to sort? objects or integers array? or type 'exit' to exit\n")

    if temp1.lower() == 'objects':
        lyst = list()
        for x in range(10):
            height_random = random.choice(range(100, 200))
            weight_random = random.choice(range(20, 100))
            age_random = random.choice(range(20, 120))
            lyst.append(Person.Person(height_random, weight_random, age_random))
        pconn, cconn = Pipe()
        p = Process(target=quicksortParallel, args=(lyst, cconn, n))
        p.start()

        lyst = pconn.recv()
        p.join()
        print("\n\n", lyst)

    elif temp1.lower() == 'integers':
        lyst = random.sample(range(10000), 100)
        pconn, cconn = Pipe()
        p = Process(target=quicksortParallel, args=(lyst, cconn , n))
        p.start()
        lyst = pconn.recv()
        p.join()
        print("\n\n", lyst)

    elif temp1.lower() == 'exit':
        print("See ya! :)")

def quicksort(lyst):

	if len(lyst) <= 1:
		return lyst
	pivot = lyst.pop(random.randint(0, len(lyst)-1))
	return quicksort([x for x in lyst if x < pivot]) + [pivot] + quicksort([x for x in lyst if x > pivot])

def quicksortParallel(lyst, conn, procNum):

	if procNum <= 0 or len(lyst) <= 1:
		conn.send(quicksort(lyst))
		conn.close()
		return

	pivot = lyst.pop(random.randint(0, len(lyst)-1))

	leftSide = [x for x in lyst if x< pivot]   #change the name of class attribute here
	rightSide = [x for x in lyst if x> pivot]   #change the name of class attribute here

	pconnLeft, cconnLeft = Pipe()
	leftProc = Process(target=quicksortParallel, args=(leftSide, cconnLeft, procNum - 1))
	pconnRight, cconnRight = Pipe()
	rightProc = Process(target=quicksortParallel, args=(rightSide, cconnRight, procNum - 1))

	leftProc.start()
	rightProc.start()

	conn.send(pconnLeft.recv() + [pivot] + pconnRight.recv())
	conn.close()

	leftProc.join()
	rightProc.join()


if __name__ == '__main__':
    main()