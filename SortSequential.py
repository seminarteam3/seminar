import random
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure, show
from threading import Thread
import time
import plotly.express as px
import pandas as pd

import pylab as plt
from matplotlib.pyplot import figure, show
import numpy as np

class SortSeq:
    x_vals = ['Insertion Sort', 'Bubble Sort', 'Selection Sort']
    y_vals = []
    processes = []

    index = 0
    height = 0

    startBubbleSort = 0
    startSelectionSort = 0
    startInsertionSort = 0

    bubbleSortTime = 0
    selectionSortTime = 0
    insertionSortTime = 0

    bubbleSortFinish = False
    selectionSortFinish = False
    insertionSortFinish = False

    # labels for bars
    tick_label = ['Bubble Sort', 'Selection Sort', 'Insertion Sort']

    # x-coordinates of left sides of bars
    left = 1

    def bubbleSort(self, arr):
        startBubbleSort = time.time()
        a = time.perf_counter()
        n = len(arr)

        # Traverse through all array elements
        for i in range(n):
            # Last i elements are already in place
            for j in range(0, n - i - 1):

                # traverse the array from 0 to n-i-1
                # Swap if the element found is greater
                # than the next element
                if arr[j] > arr[j + 1]:
                    arr[j], arr[j + 1] = arr[j + 1], arr[j]

        end = time.time()
        b = time.perf_counter()
        self.y_vals.append(b - a)
        self.bubbleSortTime = end - startBubbleSort
        print("Bubble sort finished !\n")
        bubbleSortFinish = True

    def selectionSort(self, A):
        startSelectionSort = time.time()
        a = time.perf_counter()
        # Traverse through all array elements
        for i in range(len(A)):
            # Find the minimum element in remaining
            # unsorted array
            min_idx = i
            for j in range(i + 1, len(A)):
                if A[min_idx] > A[j]:
                    min_idx = j

                    # Swap the found minimum element with
            # the first element
            A[i], A[min_idx] = A[min_idx], A[i]

        end = time.time()
        b = time.perf_counter()
        self.y_vals.append(b - a)
        self.selectionSortTime = end - startSelectionSort
        selectionSortFinish = True
        print("Selection sort finished !\n")

    # Python3 program to sort an array
    # using bucket sort
    def insertionSort(self, b):
        startInsertionSort = time.time()
        a = time.perf_counter()
        for i in range(1, len(b)):
            up = b[i]
            j = i - 1
            while j >= 0 and b[j] > up:
                b[j + 1] = b[j]
                j -= 1
            b[j + 1] = up
        end = time.time()
        b = time.perf_counter()
        self.y_vals.append(b - a)
        self.insertionSortTime = end - startInsertionSort
        insertionSortFinish = True
        print("Insertion sort finished !\n")
        return b

    '''def a(self):
        plt.ion()
        X = np.linspace(0, 4095, 16)
        Y = np.linspace(0, 10000, 16)
        f, axarr = plt.subplots(4, sharex=True)
        graph_low, = axarr[0].plot(X, Y, label='SomeLabel')
        graph_low.set_ydata(Z)'''

    def tryGraph(self):

        left = [1]
        tick_label = ['Bubble Sort']

        plt.bar(left, self.height, tick_label=tick_label,
                width=0.8, color=['red'])

        # naming the x-axis
        plt.xlabel('Sort Type')
        # naming the y-axis
        plt.ylabel('Time')
        # plot title
        plt.title('Comparing Sort Run Times')

        # function to show the plot
        plt.show()

        for i in range(0,100):
            ax = plt.bar(left, self.height)
            ax.patches[0].set_height(i)


    def otherGraph(self):
        y_vals_1 = [[], [], []]
        mx = int(max(self.y_vals) + 1)

        for a in range(mx * 2):
            if (self.y_vals[0] <= (a / 2)):
                y_vals_1[0].append(self.y_vals[0])
            else:
                y_vals_1[0].append(a / 2)

            if (self.y_vals[1] <= (a / 2)):
                y_vals_1[1].append(self.y_vals[1])
            else:
                y_vals_1[1].append(a / 2)

            if (self.y_vals[2] <= (a / 2)):
                y_vals_1[2].append(self.y_vals[2])
            else:
                y_vals_1[2].append(a / 2)

        tmp = [[], [], []]
        for a in range(len(y_vals_1[0])):
            tmp[0].append('Insertion Sort')
            tmp[1].append(y_vals_1[0][a])
            tmp[2].append(a / 2)

            tmp[0].append('Bubble Sort')
            tmp[1].append(y_vals_1[1][a])
            tmp[2].append(a / 2)

            tmp[0].append('Selection Sort')
            tmp[1].append(y_vals_1[2][a])
            tmp[2].append(a / 2)

        df = pd.DataFrame()
        df['Algos'] = tmp[0]
        df['Time(s)'] = tmp[1]
        df['Time'] = tmp[2]

        fig = px.bar(df, x="Algos", y="Time(s)", color="Algos", animation_frame="Time", animation_group="Time(s)",
                     range_y=[0, mx])
        fig.show()

    def createGraph(self):
        
        left = [1, 2, 3]
    
        # heights of bars
        height = [self.bubbleSortTime, self.selectionSortTime, self.insertionSortTime]

        # plotting a bar chart
        plt.bar(left, height, tick_label=self.tick_label,
                width=0.8, color=['red', 'green', 'blue'])

        # naming the x-axis
        plt.xlabel('Sort Type')
        # naming the y-axis
        plt.ylabel('Time')
        # plot title
        plt.title('Comparing Sort Run Times')

        # function to show the plot
        plt.show()


if __name__ == '__main__':
    print("wait for a while ... ")
    sort = SortSeq()
    lst = list()
    for i in range(5000):
        lst.append(random.randrange(0, 10000, 1))

    print(lst)

    #t4 = Thread(target=sort.createGraph)
    t1 = Thread(target=sort.bubbleSort, args=[lst])
    t2 = Thread(target=sort.selectionSort, args=[lst])
    t3 = Thread(target=sort.insertionSort, args=[lst])

    t1.start()
    t2.start()
    t3.start()
    #t4.start()

    #while not sort.bubbleSortFinish and not sort.selectionSortFinish and not sort.insertionSortFinish:
        #sort.createGraph()
    #sort.tryGraph()

    t1.join()
    t2.join()
    t3.join()
    #t4.join()
    #sort.createGraph()
    print(lst)
    print("Times:\n1:{} 2:{} 3:{} ".format(sort.bubbleSortTime, sort.selectionSortTime, sort.insertionSortTime))
    sort.otherGraph()
    print("---------Done----------")




#////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    '''lyst1 = random.sample(range(10000), 10000)
    lyst2 = []
    lyst3 = []
    print

    for a in range(len(lyst1)):
        lyst2.append(lyst1[a])
        lyst3.append(lyst1[a])'''


