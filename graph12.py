import threading
import multiprocessing
import random
import time
import numpy as np
import plotly.express as px
import pandas as pd

class RepeatedTimer(object):
  def __init__(self, interval, function, *args, **kwargs):
    self._timer = None
    self.interval = interval
    self.function = function
    self.args = args
    self.kwargs = kwargs
    self.is_running = False
    self.next_call = time.time()
    self.start()

  def _run(self):
    self.is_running = False
    self.start()
    self.function(*self.args, **self.kwargs)

  def start(self):
    if not self.is_running:
      self.next_call += self.interval
      self._timer = threading.Timer(self.next_call - time.time(), self._run)
      self._timer.start()
      self.is_running = True

  def stop(self):
    self._timer.cancel()
    self.is_running = False



def insertion_sort(InputList):
    for i in range(1, len(InputList)):
        j = i-1
        nxt_element = InputList[i]
        
        while (InputList[j] > nxt_element) and (j >= 0):
            InputList[j+1] = InputList[j]
            j=j-1
        InputList[j+1] = nxt_element

def bubblesort(list):

    for iter_num in range(len(list)-1,0,-1):
        for idx in range(iter_num):
            if list[idx]>list[idx+1]:
                temp = list[idx]
                list[idx] = list[idx+1]
                list[idx+1] = temp

def selection_sort(input_list):

    for idx in range(len(input_list)):
        min_idx = idx

        for j in range( idx +1, len(input_list)):
            if input_list[min_idx] > input_list[j]:
                min_idx = j

        input_list[idx], input_list[min_idx] = input_list[min_idx], input_list[idx]

lyst1 = random.sample(range(1000000),7000)
lyst2 = []
lyst3 = []
print

for a in range(len(lyst1)):
    lyst2.append(lyst1[a])
    lyst3.append(lyst1[a])

x_vals = ['Insertion Sort', 'Bubble Sort', 'Selection Sort']
y_vals = []
processes = []


print("wait for a while ... ")


a = time.perf_counter()
insertion_sort(lyst1)
b = time.perf_counter()
y_vals.append(b-a)

a = time.perf_counter()
bubblesort(lyst2)
b = time.perf_counter()
y_vals.append(b-a)

a = time.perf_counter()
selection_sort(lyst3)
b = time.perf_counter()
y_vals.append(b-a)

y_vals_1 = [[],[],[]]
mx = int(max(y_vals)+1)

for a in range(mx*2):
	if (y_vals[0] <= (a/2)):
		y_vals_1[0].append(y_vals[0])
	else:
		y_vals_1[0].append(a/2)

	if (y_vals[1] <= (a/2)):
		y_vals_1[1].append(y_vals[1])
	else:
		y_vals_1[1].append(a/2)

	if (y_vals[2] <= (a/2)):
		y_vals_1[2].append(y_vals[2])
	else:
		y_vals_1[2].append(a/2)

tmp = [[],[],[]]
for a in range(len(y_vals_1[0])):
	tmp[0].append('Insertion Sort')
	tmp[1].append(y_vals_1[0][a])
	tmp[2].append(a/2)

	tmp[0].append('Bubble Sort')
	tmp[1].append(y_vals_1[1][a])
	tmp[2].append(a/2)

	tmp[0].append('Selection Sort')
	tmp[1].append(y_vals_1[2][a])
	tmp[2].append(a/2)

df = pd.DataFrame()
df['Algos'] = tmp[0]
df['Time(s)'] = tmp[1]
df['Time'] = tmp[2]


fig = px.bar(df, x="Algos", y="Time(s)", color="Algos", animation_frame="Time", animation_group="Time(s)", range_y=[0,mx])
fig.show()
